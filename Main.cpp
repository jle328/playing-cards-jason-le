//Playing Cards
//Jason Le

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

struct Card
{
	Rank rank;
	Suit suit;
};

enum Rank
{
	two = 2, three, four, five, six, seven, eight, nine, ten, jack, queen, king, ace
};

enum Suit
{
	heart, spade, club, diamond
};

int main()
{

	_getch();
	return 0;
}